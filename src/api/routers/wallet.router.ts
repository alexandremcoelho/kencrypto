import { Router } from "express";
import WalletControler from "../controllers/wallet.controller";
import passport from "passport";

const router = Router();

export default () => {
  const whalletController = new WalletControler();
  router.get(
    "/wallet",
    passport.authenticate("jwt", { session: false }),
    whalletController.check
  );

  return router;
};
