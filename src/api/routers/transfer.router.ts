import { Router } from "express";
import TransferControler from "../controllers/transfer.controler";
import passport from "passport";
import { CheckTranfer } from "../midlewares/checkTransfer";

const router = Router();

export default () => {
  const transferController = new TransferControler();
  router.post(
    "/transfer",
    passport.authenticate("jwt", { session: false }),
    CheckTranfer,
    transferController.transfer
  );

  return router;
};
