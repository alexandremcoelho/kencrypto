import { Router } from "express";
import QuotesControler from "../controllers/quotes.controller";

const router = Router();

export default () => {
  const quotesController = new QuotesControler();
  router.get("/quotes", quotesController.list);

  return router;
};
