import { Router } from "express";
import DepositControler from "../controllers/deposit.controller";
import passport from "passport";
import { CheckDeposit } from "../midlewares/checkdeposit";

const router = Router();

export default () => {
  const depositController = new DepositControler();
  router.post(
    "/deposit",
    passport.authenticate("jwt", { session: false }),
    CheckDeposit,
    depositController.deposit
  );

  return router;
};
