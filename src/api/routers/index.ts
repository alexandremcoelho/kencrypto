import { Express } from "express";
import UserRouter from "./user.router";
import LoginRouter from "./login.router";
import QuotesRouter from "./quotes.router";
import WalletRouter from "./wallet.router";
import DepositRouter from "./deposit.router";
import TransferRouter from "./transfer.router";
import { Strategy } from "../midlewares";
import passport from "passport";

export default (app: Express) => {
  passport.use(Strategy());

  const userrouter = UserRouter();
  const loginRouter = LoginRouter();
  const quoteRouter = QuotesRouter();
  const walletRouter = WalletRouter();
  const depositRouter = DepositRouter();
  const transferRouter = TransferRouter();

  app.use("/api", userrouter);
  app.use("/api", loginRouter);
  app.use("/api", quoteRouter);
  app.use("/api", walletRouter);
  app.use("/api", depositRouter);
  app.use("/api", transferRouter);
};
