import { Router } from "express";
import UserController from "../controllers/user.controller";
import { CheckUserFields, UniqueUserVerification } from "../midlewares";
const router = Router();

export default (): Router => {
  const usercontroller = new UserController();
  router.get("/users", usercontroller.list);
  router.post(
    "/users",
    CheckUserFields,
    UniqueUserVerification,
    usercontroller.create
  );

  return router;
};
