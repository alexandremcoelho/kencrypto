import { Request, Response } from "express";
import { MicroAPI } from "project5/src/";

export default class QuotesController {
  list = async (req: Request, res: Response) => {
    let currency: any = req.query.currency;
    let coinList;
    if (currency) {
      coinList = currency.split(",");
    } else {
      coinList = ["BTC", "ETH", "DOGE", "ADA", "LTC"];
    }

    const api = new MicroAPI("e95e73ab-f34c-4d04-bd98-04cfd2cbea4c");
    const data = await api.quotes(coinList);
    res.status(200).send(data);
  };
}
