import { Request, Response } from "express";
import { User } from "../../entities";
import { getRepository } from "typeorm";
export default class WalletController {
  check = async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    const userRequest = req.user;

    const finduser = await userRepository.find(req.user);
    const user = await userRepository.findOneOrFail(finduser[0].id, {
      relations: ["wallet"],
    });

    let serializer = {
      currency: {
        USD: user.wallet.USD,
        BTC: user.wallet.BTC,
        ETH: user.wallet.ETH,
        DOGE: user.wallet.DOGE,
        ADA: user.wallet.ADA,
        LTC: user.wallet.LTC,
      },
      id: user.wallet.id,
    };

    res.status(200).send(serializer);
  };
}
