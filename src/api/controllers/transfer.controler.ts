import { Request, Response } from "express";
import { User, Wallet } from "../../entities";
import { getRepository } from "typeorm";
import { MicroAPI } from "project5/src/";

export default class TransferControler {
  transfer = async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    const walletRepository = getRepository(Wallet);

    const userRequest = req.user;
    const finduser = await userRepository.find(req.user);
    const user = await userRepository.findOneOrFail(finduser[0].id, {
      relations: ["wallet"],
    });

    const wallet = await walletRepository.find(user.wallet);
    const { amount, from, to } = req.body;
    const api = new MicroAPI("e95e73ab-f34c-4d04-bd98-04cfd2cbea4c");
    let data = await api.conversion(from, amount, to);
    // const coins = [ "USD", "BTC", "ETH", "DOGE", "ADA", "LTC"];
    switch (from) {
      case "USD":
        if (amount > wallet[0].USD) return res.sendStatus(400);
        wallet[0].USD -= amount;
        switch (to) {
          case "USD":
            wallet[0].USD += data.data.quote.USD.price;
            break;
          case "BTC":
            wallet[0].BTC += data.data.quote.BTC.price;
            break;
          case "ETH":
            wallet[0].ETH += data.data.quote.ETH.price;
            break;
          case "DOGE":
            wallet[0].DOGE += data.data.quote.DOGE.price;
            break;
          case "ADA":
            wallet[0].ADA += data.data.quote.ADA.price;
            break;
          case "LTC":
            wallet[0].LTC += data.data.quote.LTC.price;
            break;
        }
        break;
      case "BTC":
        if (amount > wallet[0].BTC) return res.sendStatus(400);
        wallet[0].BTC -= amount;
        switch (to) {
          case "USD":
            wallet[0].USD += data.data.quote.USD.price;
            break;
          case "BTC":
            wallet[0].BTC += data.data.quote.BTC.price;
            break;
          case "ETH":
            wallet[0].ETH += data.data.quote.ETH.price;
            break;
          case "DOGE":
            wallet[0].DOGE += data.data.quote.DOGE.price;
            break;
          case "ADA":
            wallet[0].ADA += data.data.quote.ADA.price;
            break;
          case "LTC":
            wallet[0].LTC += data.data.quote.LTC.price;
            break;
        }
        break;
      case "ETH":
        if (amount > wallet[0].ETH) return res.sendStatus(400);
        wallet[0].ETH -= amount;
        switch (to) {
          case "USD":
            wallet[0].USD += data.data.quote.USD.price;
            break;
          case "BTC":
            wallet[0].BTC += data.data.quote.BTC.price;
            break;
          case "ETH":
            wallet[0].ETH += data.data.quote.ETH.price;
            break;
          case "DOGE":
            wallet[0].DOGE += data.data.quote.DOGE.price;
            break;
          case "ADA":
            wallet[0].ADA += data.data.quote.ADA.price;
            break;
          case "LTC":
            wallet[0].LTC += data.data.quote.LTC.price;
            break;
        }
        break;
      case "DOGE":
        if (amount > wallet[0].DOGE) return res.sendStatus(400);
        wallet[0].DOGE -= amount;
        switch (to) {
          case "USD":
            wallet[0].USD += data.data.quote.USD.price;
            break;
          case "BTC":
            wallet[0].BTC += data.data.quote.BTC.price;
            break;
          case "ETH":
            wallet[0].ETH += data.data.quote.ETH.price;
            break;
          case "DOGE":
            wallet[0].DOGE += data.data.quote.DOGE.price;
            break;
          case "ADA":
            wallet[0].ADA += data.data.quote.ADA.price;
            break;
          case "LTC":
            wallet[0].LTC += data.data.quote.LTC.price;
            break;
        }
        break;
      case "ADA":
        if (amount > wallet[0].ADA) return res.sendStatus(400);
        wallet[0].ADA -= amount;
        switch (to) {
          case "USD":
            wallet[0].USD += data.data.quote.USD.price;
            break;
          case "BTC":
            wallet[0].BTC += data.data.quote.BTC.price;
            break;
          case "ETH":
            wallet[0].ETH += data.data.quote.ETH.price;
            break;
          case "DOGE":
            wallet[0].DOGE += data.data.quote.DOGE.price;
            break;
          case "ADA":
            wallet[0].ADA += data.data.quote.ADA.price;
            break;
          case "LTC":
            wallet[0].LTC += data.data.quote.LTC.price;
            break;
        }
        break;
      case "LTC":
        if (amount > wallet[0].LTC) return res.sendStatus(400);
        wallet[0].LTC -= amount;
        switch (to) {
          case "USD":
            wallet[0].USD += data.data.quote.USD.price;
            break;
          case "BTC":
            wallet[0].BTC += data.data.quote.BTC.price;
            break;
          case "ETH":
            wallet[0].ETH += data.data.quote.ETH.price;
            break;
          case "DOGE":
            wallet[0].DOGE += data.data.quote.DOGE.price;
            break;
          case "ADA":
            wallet[0].ADA += data.data.quote.ADA.price;
            break;
          case "LTC":
            wallet[0].LTC += data.data.quote.LTC.price;
            break;
        }
        break;
    }

    const updatedwallet = await walletRepository.save(wallet);
    let serializer = {
      currency: {
        USD: updatedwallet[0].USD,
        BTC: updatedwallet[0].BTC,
        ETH: updatedwallet[0].ETH,
        DOGE: updatedwallet[0].DOGE,
        ADA: updatedwallet[0].ADA,
        LTC: updatedwallet[0].LTC,
      },
      id: updatedwallet[0].id,
    };

    res.status(200).send(serializer);
  };
}
