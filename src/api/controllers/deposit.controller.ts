import { Request, Response } from "express";
import { User, Wallet } from "../../entities";
import { getRepository } from "typeorm";

export default class DepositControler {
  deposit = async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    const walletRepository = getRepository(Wallet);

    const userRequest = req.user;
    const finduser = await userRepository.find(req.user);
    const user = await userRepository.findOneOrFail(finduser[0].id, {
      relations: ["wallet"],
    });

    const wallet = await walletRepository.find(user.wallet);
    const { amount } = req.body;
    wallet[0].USD = wallet[0].USD + amount;
    const updatedwallet = await walletRepository.save(wallet);

    let serializer = {
      currency: {
        USD: updatedwallet[0].USD,
        BTC: updatedwallet[0].BTC,
        ETH: updatedwallet[0].ETH,
        DOGE: updatedwallet[0].DOGE,
        ADA: updatedwallet[0].ADA,
        LTC: updatedwallet[0].LTC,
      },
      id: updatedwallet[0].id,
    };

    res.status(200).send(serializer);
  };
}
