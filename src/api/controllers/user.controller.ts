import { Request, Response } from "express";
import { User, Wallet } from "../../entities";
import { getRepository } from "typeorm";

export default class UserController {
  create = async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    const walletRepository = getRepository(Wallet);
    const { username, password } = req.body;

    const wallet = new Wallet();
    const createdWallet = await walletRepository.save(wallet);

    const user = new User(username, password);
    user.wallet = createdWallet;

    const createduser = await userRepository.save(user);
    res.status(201).send({ username: createduser.username });
  };

  list = async (req: Request, res: Response) => {
    const userRepository = getRepository(User);
    const users: Array<User> = await userRepository.find();

    res.status(200).send(users);
  };
}
