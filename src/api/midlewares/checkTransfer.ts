import { Request, Response, NextFunction } from "express";

export const CheckTranfer = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const recievedFields = Object.keys(req.body);
  let allFields = ["amount", "from", "to"];
  let missing = {};
  for (let i = 0; i < allFields.length; i++) {
    if (!recievedFields.includes(allFields[i])) {
      let missingField = allFields[i];
      missing = {
        ...missing,
        ...{ missingField: "Missing field " + missingField },
      };
    }
  }
  if (Object.keys(missing).length > 0) {
    return res.status(400).send(missing);
  }

  const { from, to } = req.body;
  const coins = ["BTC", "ETH", "DOGE", "ADA", "LTC", "USD"];
  if (coins.includes(from) && coins.includes(to)) {
    return next();
  } else {
    return res.sendStatus(404);
  }
};
