import { Request, Response, NextFunction } from "express";
import { User } from "../../entities";
import { getRepository } from "typeorm";

export const UniqueUserVerification = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const { username } = req.body;
  const userRepository = getRepository(User);

  const user = await userRepository.find({ username: username });

  if (user.length != 0) return res.sendStatus(422);

  return next();
};
