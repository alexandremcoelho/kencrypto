import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  BeforeUpdate,
  OneToOne,
  JoinColumn,
} from "typeorm";
import bcrypt from "bcrypt";
import { Wallet } from "./wallet";

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  username!: string;

  @Column()
  password!: string;

  @OneToOne(() => Wallet)
  @JoinColumn()
  wallet!: Wallet;

  constructor(username: string, password: string) {
    this.username = username;
    this.password = password;
  }

  @BeforeInsert()
  @BeforeUpdate()
  hashpassword() {
    this.password = bcrypt.hashSync(this.password, 8);
  }
}
