import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Wallet {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  USD!: number;

  @Column()
  BTC!: number;

  @Column()
  ETH!: number;

  @Column()
  DOGE!: number;

  @Column()
  ADA!: number;

  @Column()
  LTC!: number;

  constructor() {
    this.USD = 0;
    this.BTC = 0;
    this.ETH = 0;
    this.DOGE = 0;
    this.ADA = 0;
    this.LTC = 0;
  }
}
