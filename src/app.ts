import express from "express";
import { createConnection } from "typeorm";
import RouterBuilder from "./api/routers";
import databaseConfig from "./config/database";
import passport from "passport";

const app = express();
app.use(passport.initialize());
app.use(express.json());

const PORT = 8000;

RouterBuilder(app);

createConnection(databaseConfig)
  .then((_conection) => {
    app.listen(PORT, () => {
      console.log(`Running at http://localhost:${PORT}`);
    });
  })
  .catch((err) => {
    process.exit(1);
  });
