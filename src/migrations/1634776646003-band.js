const { MigrationInterface, QueryRunner } = require("typeorm");

module.exports = class band1634776646003 {
    name = 'band1634776646003'

    async up(queryRunner) {
        await queryRunner.query(`CREATE TABLE "user" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "username" varchar NOT NULL, "password" varchar NOT NULL)`);
        await queryRunner.query(`CREATE TABLE "wallet" ("id" integer PRIMARY KEY AUTOINCREMENT NOT NULL, "USD" integer NOT NULL, "BTC" integer NOT NULL, "ETH" integer NOT NULL, "DOGE" integer NOT NULL, "ADA" integer NOT NULL, "LTC" integer NOT NULL)`);
    }

    async down(queryRunner) {
        await queryRunner.query(`DROP TABLE "wallet"`);
        await queryRunner.query(`DROP TABLE "user"`);
    }
}
